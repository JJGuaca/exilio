var click = 0;
let btnCambio = document.getElementById('cambio');
window.onload = function(){
    var boton = document.querySelectorAll('.ancla');
    for(var i=0; i<boton.length; i++){
        boton[i].addEventListener('click', function(){
            var destino = this.href.split('#');
            var punto = document.getElementById(destino[1]);
            var islas = document.querySelectorAll('.islas');
            var pos2 = document.querySelector('.posc2');
            var pos = document.querySelector('.posc');
            var isla0 = document.getElementById('isla0');
            isla0.style.display = 'none';
            if(click > 0){
                pos2.classList.toggle('posc2');
                pos.classList.toggle('posc');
                for(var j =0; j<islas.length;j++){
                    if(islas[j].id == destino[1]){
                       punto.classList.toggle('posc2');                    
                   }
                }
                this.classList.toggle('posc');
                
            }else{
                //pos.classList.toggle('posc2');
                for(var j =0; j<islas.length;j++){
                    if(islas[j].id == destino[1]){
                       punto.classList.toggle('posc2');                    
                   }
                }
                this.classList.toggle('posc');
                click++;
            }
           
        })
    }
}
btnCambio.addEventListener('click', function(){
    this.children[0].classList.toggle('bi-toggle2-on');
});

const  data = [
    {
        "id":"modal1",
        "categoria":"voces",
        "titulo":"prueba1",
        "items":[
            {
                "titulo":"prueba1",
                "cuerpo":"It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions."
            },
            {
                "titulo":"prueba2",
                "cuerpo":"It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions."
            },
            {
                "titulo":"prueba3",
                "cuerpo":"It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions."
            }
        ]

    },
    {
        "id":"modal2",
        "categoria":"voces",
        "titulo":"prueba2",
        "items":[
            {
                "titulo":"prueba1",
                "cuerpo":"It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions."
            },
            {
                "titulo":"prueba2",
                "cuerpo":"It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions."
            }
        ]

    },
    {
      "id":"modal3",
      "categoria":"voces",
      "titulo":"prueba3",
      "items":[
          {
              "titulo":"prueba1",
              "cuerpo":"It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions."
          }
      ]

  },
];
let navegado = [];
document.addEventListener("DOMContentLoaded", function() {
    datos = data;
    appendElementos(datos);
    appendModl(datos);
 });
 function appendModl(datos){
    const modales = document.querySelector('.espacio');
    let datosNodos = [];
    for (dato of datos) {
      datosNodos.push(createCModal(dato));
    }
    console.log(datosNodos);
    modales.innerHTML = datosNodos.join(" ");
    // let x = document.getElementsByClassName("modal");
    // let i;
    // // for (i = 0; i < x.length; i++) {
    // //   x[i].addEventListener('click', (x,i)=>elementClickHandler(x[i]), false);
    // // }
  }
  function appendElementos(datos) {
    const btnes = document.querySelector('.modales');
    let elementoNodes = [];  
    for (dato of datos) {
        elementoNodes.push(crearBModal(dato));
    }
    // btnes.innerHTML = elementoNodes.join(" ");
    for(var i =0; i<elementoNodes.length; i++){
      btnes.innerHTML+=elementoNodes[i];
    }
  }
  function crearBModal(film) {
    return `<div class="elemento card" style="width: 50%;">
              <a  data-bs-toggle="modal" data-bs-target="#${film.id}" class="">
              <img src="${film.image}" class="card-img-top" alt="${film.image}">
              </a>
            </div>`;
  }
  function createCModal(film){
    var cadena2 =  crearItems(film.items);
    var cadena =`
    <div class="modal fade" id="${film.id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">${film.titulo}</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="container">`;
    var cadena3=`
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>`;
  var cadenaFull = cadena+cadena2+cadena3;
  return cadenaFull;
  }
  function crearItems(valor){
    var cadena = '';
    for(var i=0; i< valor.length;i++){
      cadena+= `
      <div class="accordion-item"> 
        <h2 class="accordion-header" id="tituloAcordeon`+i+`">
           <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#cerrarAcordeon`+i+`" aria-expanded="true" aria-controls="cerrarAcordeon`+i+`">${valor[i].titulo}</button>
        </h2>
      <div id="cerrarAcordeon`+i+`" class="accordion-collapse collapse" aria-labelledby="tituloAcordeon`+i+`" data-bs-parent="#accordionExample">
         <div class="accordion-body">
            <strong>Descripción: </strong>${valor[i].cuerpo}
          </div>
      </div></div>`;
    }
   return '<div class="accordion" id="accordionExample">'+cadena+'</div>';
  }

